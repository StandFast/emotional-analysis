import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.nlp.v20190408.NlpClient;
import com.tencentcloudapi.nlp.v20190408.models.SentimentAnalysisRequest;
import com.tencentcloudapi.nlp.v20190408.models.SentimentAnalysisResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException {

//        Document document = Jsoup.connect("https://api.bilibili.com/x/player/pagelist?bvid=BV1dT411p7x1&jsonp=jsonp").get();
//        System.out.println(document);
        String url = "https://www.bilibili.com/video/BV1dT411p7x1/?spm_id_from=333.1007.tianma.13-3-49.click";
        url = url.substring(url.indexOf("video/")+6);
        String vid = url.substring(0, url.indexOf("/"));

        HttpClient httpClient = HttpClients.createDefault();
        String cidUrl = "https://api.bilibili.com/x/player/pagelist?bvid="+vid+"&jsonp=jsonp";
        HttpGet httpGet = new HttpGet(cidUrl);
        String res = httpClient.execute(httpGet, response -> EntityUtils.toString(response.getEntity()));
        JSONObject jsonObject = JSON.parseObject(res);
        String cid = jsonObject.getJSONArray("data").getJSONObject(0).getString("cid");

        Document document = Jsoup.connect("https://comment.bilibili.com/"+cid+".xml").get();
        Elements select = document.select("d");
        for(int i = 0; i < select.size(); i++){
            System.out.println(select.get(i).text());
        }

//        System.out.println(document.html());
//        Elements select = document.select(".comment-main-list-item-content-comment");
//        for (int i = 0; i < select.size(); i++) {
//            Element element = select.get(i);
//            Element span = element.select("span").last();
//            System.out.println(span.text());
//        }

    }
}
