package cn.tworice.vo;

import lombok.Data;

@Data
public class Senta {
    private String line;
    private Integer state;

    public Senta(String line, Integer state) {
        this.line = line;
        this.state = state;
    }
}
