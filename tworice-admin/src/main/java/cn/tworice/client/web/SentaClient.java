package cn.tworice.client.web;


import cn.tworice.common.vo.RequestResult;
import cn.tworice.nlp.TencentNLP;
import cn.tworice.vo.Senta;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/client/senta")
@RestController
@CrossOrigin
public class SentaClient {

    private TencentNLP tencentNLP;

    @PostConstruct
    public void init(){
        tencentNLP = new TencentNLP();
    }

    /**
     * 读取B站视频弹幕,并分析弹幕积极性
     * @param url B站视频链接
     * @return cn.tworice.common.vo.RequestResult
     * @author 二饭 [2023/4/12]
     **/
    @PostMapping("")
    public RequestResult senta(String url) throws IOException {
        List<Senta> sentaList = new ArrayList<>();
        url = url.substring(url.indexOf("video/")+6);
        String vid = url.substring(0, url.indexOf("/"));

        HttpClient httpClient = HttpClients.createDefault();
        String cidUrl = "https://api.bilibili.com/x/player/pagelist?bvid="+vid+"&jsonp=jsonp";
        HttpGet httpGet = new HttpGet(cidUrl);
        String res = httpClient.execute(httpGet, response -> EntityUtils.toString(response.getEntity()));
        JSONObject jsonObject = JSON.parseObject(res);
        String cid = jsonObject.getJSONArray("data").getJSONObject(0).getString("cid");

        Document document = Jsoup.connect("https://comment.bilibili.com/"+cid+".xml").get();
        Elements select = document.select("d");
        for(int i = 0; i < select.size() && i<20; i++){
            String s = select.get(i).text();
            if (s.trim().length() > 4) {
                int senta = tencentNLP.senta(s.trim());
                sentaList.add(new Senta(s, senta));
            } else {
                sentaList.add(new Senta(s, 2));
            }
        }
        return RequestResult.success().appendData("sentaList", sentaList);
    }

    @PostMapping("txt")
    public RequestResult senta(MultipartFile file) throws IOException {

        List<Senta> sentaList = new ArrayList<>();

        //起手转成字符流
        InputStream is = file.getInputStream();
        InputStreamReader isReader = new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isReader);
        //循环逐行读取
        while (br.ready()) {
            String s = br.readLine();
            if (s.trim().length() > 4) {
                int senta = tencentNLP.senta(s.trim());
                sentaList.add(new Senta(s, senta));
            } else {
                sentaList.add(new Senta(s, 2));
            }
        }
        //关闭流
        br.close();

        return RequestResult.success().appendData("sentaList", sentaList);
    }

//    @PostMapping("content")
//    public RequestResult senta(String content) throws IOException {
//        return RequestResult.success().appendData("senta", new Senta(content,tencentNLP.senta(content)));
//    }

    @PostMapping("smzdm")
    public RequestResult reptile(String url) throws IOException {
        if (url.trim().isEmpty()) {
            return RequestResult.error();
        }
        Document document = Jsoup.connect(url).get();
        List<Senta> sentaList = new ArrayList<>();
        Elements select = document.select(".comment-main-list-item-content-comment");
        for (int i = 0; i < select.size(); i++) {
            Element element = select.get(i);
            Element span = element.select("span").last();
            String s = span.text();
            if (s.trim().length() > 4) {
                int senta = tencentNLP.senta(s.trim());
                sentaList.add(new Senta(s, senta));
            }else{
                sentaList.add(new Senta(s, 2));
            }
        }
        return RequestResult.success().appendData("sentaList", sentaList);
    }
}
